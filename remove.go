package main

import (
	"fmt"
	"strconv"
	"strings"
)

func removeHandler(hd handlerData) (ctx *context) {
	var (
		arg = hd.arg
		u   = hd.user
		c   = hd.channel
		g   = hd.guild
		s   = hd.session
		b   strings.Builder
	)

	vsc := getUserVoiceChannel(u, g, s)
	if vsc == nil {
		fmt.Fprint(&b, "You have to be in a voice channel!\n")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	selected, err := strconv.Atoi(arg)
	if err != nil {
		fmt.Fprint(&b, "Couldn't parse selected index!\n")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	key := playerKey{ChannelID: vsc.ID, GuildID: g.ID}
	playersRWMutex.RLock()
	player, ok := players[key]
	playersRWMutex.RUnlock()
	if ok {
		fmt.Fprintf(&b, "Removing #%d from the queue...\n", selected)
		err := player.Remove(selected)
		if err != nil {
			fmt.Fprintf(&b, "Remove failed: %s\n", err)
		}
	} else {
		fmt.Fprint(&b, "SIKE! You don't have permissions to remove anything from the queue ;)\n")
	}
	s.ChannelMessageSend(c.ID, b.String())

	return
}

func init() {
	handlers["removeHandler"] = removeHandler
}
