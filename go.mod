module gitlab.com/dziku1337/ziembot

go 1.12

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/ebml-go/ebml v0.0.0-20160925193348-ca8851a10894 // indirect
	github.com/petar/GoLLRB v0.0.0-20190514000832-33fb24c13b99 // indirect
	gitlab.com/dziku1337/webm v0.0.0-20190904144712-5b9333f29c25
	gopkg.in/yaml.v2 v2.2.2
	layeh.com/gopus v0.0.0-20161224163843-0ebf989153aa
)
