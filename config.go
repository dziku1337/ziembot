package main

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type Command struct {
	Key         string   `yaml:"key"`
	Routes      []string `yaml:"routes"`
	Args        []string `yaml:"args"`
	Description string   `yaml:"description"`
}

// There are going to be custom routes read from configuration files and other stuff
type Config struct {
	discordToken string
	Prefixes     []string  `yaml:"prefixes"`
	Commands     []Command `yaml:"commands"`
	JokesURL     string    `yaml:"jokesUrl"`
}

var config *Config

func init() {
	token := os.Getenv("ZIEMBOT_TOKEN")
	if token == "" {
		log.Fatalln("No token provided. Please set ZIEMBOT_TOKEN environment variable, e.g. ZIEMBOT_TOKEN=<your_token_here>")
	}

	yamlFile, err := ioutil.ReadFile("config.yml")
	if err != nil {
		log.Fatalln("Failed to open config:", err)
	}

	config = &Config{discordToken: token}
	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		log.Fatalln("Failed to unmarshal config:", err)
	}
}
