package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
)

type Joke struct {
	Title string `json:"title"`
	Body  string `json:"body"`
	Score int    `json:"score"`
	ID    string `json:"id"`
}

var jokes []Joke

func jokeHandler(hd handlerData) (ctx *context) {
	var (
		c = hd.channel
		s = hd.session
		b strings.Builder
	)

	if len(jokes) == 0 {
		log.Println("Trying to get", config.JokesURL)
		resp, err := http.Get(config.JokesURL)
		if err != nil {
			log.Printf("Couldn't load joke database from url %s, Error: %v\n", config.JokesURL, err)
			return
		}
		defer resp.Body.Close()

		decoder := json.NewDecoder(resp.Body)
		err = decoder.Decode(&jokes)
		if err != nil {
			log.Fatalln("Error decoding jokes json", err)
			return
		}
	}

	joke := jokes[rand.Intn(len(jokes))]
	log.Printf("Random joke: %v\n", joke)

	fmt.Fprintf(&b, "**%s**\n", joke.Title)
	fmt.Fprintf(&b, "%s\n", joke.Body)
	s.ChannelMessageSend(c.ID, b.String())

	return
}

func init() {
	handlers["jokeHandler"] = jokeHandler
}
