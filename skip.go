package main

import (
	"fmt"
	"strings"
)

func skipHandler(hd handlerData) (ctx *context) {
	var (
		u = hd.user
		c = hd.channel
		g = hd.guild
		s = hd.session
		b strings.Builder
	)

	vsc := getUserVoiceChannel(u, g, s)
	if vsc == nil {
		fmt.Fprint(&b, "You have to be in a voice channel!\n")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	key := playerKey{ChannelID: vsc.ID, GuildID: g.ID}
	playersRWMutex.RLock()
	player, ok := players[key]
	playersRWMutex.RUnlock()
	if ok {
		fmt.Fprint(&b, "Skipping song...\n")
		player.Skip()
	} else {
		fmt.Fprint(&b, "SIKE! You don't have permissions to skip anything ;)\n")
	}
	s.ChannelMessageSend(c.ID, b.String())

	return
}

func init() {
	handlers["skipHandler"] = skipHandler
}
