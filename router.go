package main

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
)

type handlerData struct {
	arg     string
	message *discordgo.MessageCreate
	user    *discordgo.User
	channel *discordgo.Channel
	guild   *discordgo.Guild
	session *discordgo.Session
	data    interface{}
}

type handler func(handlerData) *context

type contextKey struct {
	UserID    string
	ChannelID string
	GuildID   string
}

type context struct {
	Timestamp  time.Time
	Data       interface{}
	HandleFunc handler
}

type Router struct {
	prefixes      []string
	routes        map[string]string
	handlers      map[string]handler
	contexts      map[contextKey]*context
	contextsMutex sync.Mutex
}

func NewRouter(config *Config) (router *Router, err error) {
	router = &Router{
		prefixes: config.Prefixes,
		routes:   map[string]string{},
		handlers: map[string]handler{},
		contexts: map[contextKey]*context{},
	}

	for _, command := range config.Commands {
		for _, route := range command.Routes {
			if _, ok := router.routes[route]; ok {
				err = fmt.Errorf("Two or more commands register route \"%s\"", route)
				return
			}
			router.routes[route] = command.Key
		}
	}

	return
}

func (r *Router) RegisterHandler(handlerKey string, handler handler) {
	r.handlers[handlerKey] = handler
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func (r *Router) onMessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	u := m.Author
	if u.ID == s.State.User.ID {
		return
	}

	// Find the channel that the message came from.
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		return
	}

	// Find the guild for that channel.
	g, err := s.State.Guild(c.GuildID)
	if err != nil {
		return
	}

	hd := handlerData{
		message: m,
		user:    u,
		channel: c,
		guild:   g,
		session: s,
	}
	key := contextKey{UserID: u.ID, ChannelID: c.ID, GuildID: g.ID}
	var handler handler

	// If content starts with one of ziembot prefixes, then it uses new context (and also wipes the old one)
	if botPrefix, ok := startsWithPrefix(m.Content, r.prefixes); !ok {
		r.contextsMutex.Lock()
		if ctx, ok := r.contexts[key]; ok {
			hd.arg = strings.TrimSpace(m.Content)
			hd.data = ctx.Data
			delete(r.contexts, key)
			r.contextsMutex.Unlock()
			handler = ctx.HandleFunc
		} else {
			r.contextsMutex.Unlock()
		}
	} else {
		// Starting new command - remove existing context
		r.contextsMutex.Lock()
		delete(r.contexts, key)
		r.contextsMutex.Unlock()

		for route, handlerKey := range r.routes {
			prefix := botPrefix + " " + route
			if strings.HasPrefix(m.Content, prefix) && (len(m.Content) == len(prefix) || m.Content[len(prefix)] == ' ') {
				hd.arg = m.Content[min(len(m.Content), len(prefix)+1):]
				hd.arg = strings.TrimSpace(hd.arg)
				handler = r.handlers[handlerKey]
				break
			}
		}
	}

	if handler != nil {
		ctx := handler(hd)
		if ctx != nil {
			r.contextsMutex.Lock()
			r.contexts[key] = ctx
			r.contextsMutex.Unlock()
		}
	}
}

func (r *Router) RegisterDiscordCallbacks(s *discordgo.Session) {
	s.AddHandler(func(sess *discordgo.Session, msg *discordgo.MessageCreate) {
		r.onMessageCreate(sess, msg)
	})
}

func startsWithPrefix(str string, prefixes []string) (prefix string, ok bool) {
	for _, prefix = range prefixes {
		if ok = strings.HasPrefix(str, prefix+" "); ok {
			return
		}
	}
	return
}
