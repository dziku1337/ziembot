package main

import (
	"fmt"
	"log"
	"strings"
)

func listHandler(hd handlerData) (ctx *context) {
	var (
		u = hd.user
		c = hd.channel
		g = hd.guild
		s = hd.session
		b strings.Builder
	)

	vsc := getUserVoiceChannel(u, g, s)
	if vsc == nil {
		fmt.Fprint(&b, "You have to be in a voice channel!\n")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	key := playerKey{ChannelID: vsc.ID, GuildID: g.ID}
	playersRWMutex.RLock()
	player, ok := players[key]
	playersRWMutex.RUnlock()
	if ok {
		log.Printf("List the queue for channel %s\n", vsc.ID)
		fmt.Fprint(&b, "Listing the queue...\n")
		playlist := player.List()
		if len(playlist) == 0 {
			log.Printf("It's empty\n")
			fmt.Fprint(&b, "It's empty\n")
		}
		for i, e := range playlist {
			log.Printf("%d\t%s\n", i, e.Title)
			fmt.Fprintf(&b, "%d\t%s\n", i, e.Title)
		}
	} else {
		fmt.Fprint(&b, "SIKE! You don't have permissions to list the queue ;)\n")
	}
	s.ChannelMessageSend(c.ID, b.String())

	return
}

func init() {
	handlers["listHandler"] = listHandler
}
