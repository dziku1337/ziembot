package main

import (
	"bytes"
	"os/exec"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func downloadYoutubeAudio(videoID string) (err error) {
	cmdParams := videoID + " -f bestaudio -x --audio-format opus --output data/%(id)s.%(ext)s"
	cmd := exec.Command("youtube-dl", strings.Split(cmdParams, " ")...)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()

	return err
}

// Look for the message sender in that guild's current voice states.
func getUserVoiceChannel(u *discordgo.User, g *discordgo.Guild, s *discordgo.Session) (vsc *discordgo.Channel) {
	for _, vs := range g.VoiceStates {
		if vs.UserID == u.ID {
			vsc, _ := s.State.Channel(vs.ChannelID)
			return vsc
		}
	}
	return vsc
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
