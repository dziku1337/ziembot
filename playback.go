package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/dziku1337/webm"
	"layeh.com/gopus"
)

const (
	frameRate       int = 48000
	channels        int = 2
	frameSize       int = 960
	maxBytes        int = frameSize * channels * 2 // frameSize * channels * 2 bytes in int16
	audioBufferSize int = 16384
)

// SendPCM will receive on the provied channel encode
// received PCM data into Opus then send that to Discordgo
func SendPCM(v *discordgo.VoiceConnection, pcm <-chan []int16, opusEncoder *gopus.Encoder) {
	if pcm == nil {
		return
	}

	for {
		// read pcm from chan, exit if channel is closed.
		recv, ok := <-pcm
		if !ok {
			log.Println("PCM Channel closed")
			return
		}

		// try encoding pcm frame with Opus
		opus, err := opusEncoder.Encode(recv, frameSize, maxBytes)
		if err != nil {
			log.Println("Error encoding PCM to Opus", err)
			return
		}

		if v.Ready == false || v.OpusSend == nil {
			// OnError(fmt.Sprintf("Discordgo not ready for opus packets. %+v : %+v", v.Ready, v.OpusSend), nil)
			// Sending errors here might not be suited
			return
		}
		// send encoded opus data to the sendOpus channel
		v.OpusSend <- opus
	}
}

func streamYoutubeAudio(videoID string, v *discordgo.VoiceConnection, stop <-chan bool, opusEncoder *gopus.Encoder) (err error) {
	ytdlCmdParams := videoID + " -f bestaudio -o -"
	ytdlCmd := exec.Command("youtube-dl", strings.Split(ytdlCmdParams, " ")...)
	ytdlPipe, _ := ytdlCmd.StdoutPipe()
	ffmpegCmdParams := fmt.Sprintf("-i - -f s16le -ar %d -ac %d -", frameRate, channels)
	ffmpegCmd := exec.Command("ffmpeg", strings.Split(ffmpegCmdParams, " ")...)
	ffmpegCmd.Stdin = ytdlPipe
	ffmpegPipe, _ := ffmpegCmd.StdoutPipe()

	err = ytdlCmd.Start()
	if err != nil {
		log.Println("Youtube-dl start error", err)
		return
	}
	err = ffmpegCmd.Start()
	if err != nil {
		log.Println("Ffmpeg start error", err)
		return
	}

	go func() {
		<-stop
		ytdlCmd.Process.Kill()
		ffmpegCmd.Process.Kill()
		ytdlCmd.Wait()
		ffmpegCmd.Wait()
	}()

	// Send "speaking" packet over the voice websocket
	err = v.Speaking(true)
	if err != nil {
		log.Println("Couldn't set speaking", err)
	}

	// Send not "speaking" packet over the websocket when we finish
	defer func() {
		err := v.Speaking(false)
		if err != nil {
			log.Println("Couldn't stop speaking", err)
		}
	}()

	send := make(chan []int16, 2)
	defer close(send)

	close := make(chan bool)
	go func() {
		SendPCM(v, send, opusEncoder)
		close <- true
	}()

	ffmpegbuf := bufio.NewReaderSize(ffmpegPipe, 16384)
	for {
		audiobuf := make([]int16, frameSize*channels)
		err = binary.Read(ffmpegbuf, binary.LittleEndian, &audiobuf)
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			log.Println("Error reading from ffmpeg pipe", err)
			return
		} else if err != nil {
			log.Println("Error reading from ffmpeg pipe", err)
			return
		}

		// Send received PCM to sendPCM
		select {
		case send <- audiobuf:
		case <-close:
			return
		}
	}
}

func streamOpusWebm(videoID string, v *discordgo.VoiceConnection, stop <-chan bool) (err error) {
	ytdlCmdParams := videoID + " -f bestaudio[acodec=opus] -o -"
	ytdlCmd := exec.Command("youtube-dl", strings.Split(ytdlCmdParams, " ")...)
	ytdlPipe, _ := ytdlCmd.StdoutPipe()
	ytdlCmd.Stderr = os.Stderr

	err = ytdlCmd.Start()
	if err != nil {
		log.Println("Youtube-dl start error", err)
		return
	}

	defer func() {
		ytdlCmd.Process.Kill()
		ytdlCmd.Wait()
		ytdlPipe.Close()
	}()

	// Send "speaking" packet over the voice websocket
	err = v.Speaking(true)
	if err != nil {
		log.Println("Couldn't set speaking", err)
	}

	// Send not "speaking" packet over the websocket when we finish
	defer func() {
		err := v.Speaking(false)
		if err != nil {
			log.Println("Couldn't stop speaking", err)
		}
	}()

	// Buffer provides an ability do download enough data ahead to prevent stuttering
	// in case youtube-dl falls behind with streaming and fails to reconnect in time
	ytdlBuffer := bufio.NewReaderSize(ytdlPipe, audioBufferSize)
	meta := &webm.WebM{}
	reader, err := webm.ParseStream(ytdlBuffer, meta)

	if err != nil {
		return fmt.Errorf("Webm parse error: %v", err)
	}

	vtrack := meta.FindFirstVideoTrack()
	atrack := meta.FindFirstAudioTrack()
	if atrack == nil {
		return fmt.Errorf("Webm does not contain audio track")
	}

	meta = nil
	log.Println("Start streaming", videoID)

	for {
		select {
		case pkt, ok := <-reader.Chan:
			if !ok {
				log.Println("Webm reader channel closed")
				return
			}

			if pkt.Timecode == webm.BadTC {
				log.Println("Bad Timecode, shutting the reader down")
				reader.Shutdown()
				return
			}

			if vtrack == nil || pkt.TrackNumber == atrack.TrackNumber {
				if v.Ready == false || v.OpusSend == nil {
					return fmt.Errorf("Discordgo not ready for opus packets. %+v : %+v", v.Ready, v.OpusSend)
				}

				v.OpusSend <- pkt.Data
			}
		case <-stop:
			reader.Shutdown()
			log.Println("Received from close signal, shutting the reader down")
			return
		}
	}
}

func playDownloadedYoutubeAudio(videoPath string, v *discordgo.VoiceConnection, stop <-chan bool, opusEncoder *gopus.Encoder) (err error) {
	ffmpegCmdParams := fmt.Sprintf("-i %s -f s16le -ar %d -ac %d -", videoPath, frameRate, channels)
	ffmpegCmd := exec.Command("ffmpeg", strings.Split(ffmpegCmdParams, " ")...)
	ffmpegPipe, _ := ffmpegCmd.StdoutPipe()
	ffmpegbuf := bufio.NewReaderSize(ffmpegPipe, 16384)

	err = ffmpegCmd.Start()
	if err != nil {
		log.Println("Ffmpeg start error", err)
		return
	}

	go func() {
		<-stop
		ffmpegCmd.Process.Kill()
		ffmpegCmd.Wait()
	}()

	// Send "speaking" packet over the voice websocket
	err = v.Speaking(true)
	if err != nil {
		log.Println("Couldn't set speaking", err)
	}

	// Send not "speaking" packet over the websocket when we finish
	defer func() {
		err := v.Speaking(false)
		if err != nil {
			log.Println("Couldn't stop speaking", err)
		}
	}()

	send := make(chan []int16, 2)
	defer close(send)

	close := make(chan bool)
	go func() {
		SendPCM(v, send, opusEncoder)
		close <- true
	}()

	for {
		audiobuf := make([]int16, frameSize*channels)
		err = binary.Read(ffmpegbuf, binary.LittleEndian, &audiobuf)
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			return
		} else if err != nil {
			log.Println("Error reading from ffmpeg pipe", err)
			return
		}

		// Send received PCM to sendPCM
		select {
		case send <- audiobuf:
		case <-close:
			return
		}
	}
}
