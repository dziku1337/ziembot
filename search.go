package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type ytFormat struct {
	Acodec     string `json:"acodec"`
	FileSize   uint   `json:"filesize"`
	Format     string `json:"format"`
	FormatID   string `json:"format_id"`
	FormatNote string `json:"format_note"`
	URL        string `json:"url"`
}

type ytSearchResultEntry struct {
	Title    string        `json:"title"`
	ID       string        `json:"id"`
	Formats  []ytFormat    `json:"formats"`
	Duration time.Duration `json:"duration"`
}

type ytSearchResult struct {
	Entries []ytSearchResultEntry `json:"entries"`
}

func searchYoutube(searchTerm string) (*ytSearchResult, error) {
	numberOfEntries := 5
	//ytSearchString := fmt.Sprintf("\"ytsearch%d:%s\"", numberOfEntries, searchTerm)
	ytSearchString := fmt.Sprintf("ytsearch%d:%s", numberOfEntries, searchTerm)
	cmd := exec.Command("youtube-dl", ytSearchString, "-J")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	//log.Println("Command:", cmd)
	err := cmd.Run()

	//log.Println("STDOUT", string(stdout.Bytes()))
	//log.Println("STDERR", string(stderr.Bytes()))

	if err != nil {
		return nil, err
	}

	result := &ytSearchResult{}
	err = json.Unmarshal(stdout.Bytes(), result)
	if err != nil {
		log.Println("Unmarshalling failed:", err)
		return nil, err
	}

	return result, nil
}

func searchYoutubeSlim(searchTerm string) (*ytSearchResult, error) {
	numberOfEntries := 5
	ytSearchString := fmt.Sprintf("ytsearch%d:%s", numberOfEntries, searchTerm)
	cmd := exec.Command("youtube-dl", ytSearchString, "--get-title", "--get-id", "-q")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		return nil, err
	}

	result := &ytSearchResult{}
	lines := strings.Split(stdout.String(), "\n")
	lines = lines[:len(lines)-1] // Ignore the last blank line

	for i := 0; i < len(lines); i += 2 {
		title := lines[i]
		id := lines[i+1]
		log.Printf("%d: Title: %s, Id: %s\n", i/2, title, id)
		entry := ytSearchResultEntry{Title: title, ID: id}
		result.Entries = append(result.Entries, entry)
	}

	return result, nil
}

var ytSearchPattern = regexp.MustCompile("<h3 class=\"yt-lockup-title \"><a href=\"/watch\\?v=([0-9A-Za-z]+)\"(.*?)>(.*?)</a><span(.*)> - Duration: (.*?).</span>")

func searchYoutubeFast(searchTerm string) (*ytSearchResult, error) {
	numberOfEntries := 5
	searchTerm = url.QueryEscape(searchTerm)
	ytEndpoint := "https://www.youtube.com/results?search_query=" + searchTerm
	resp, err := http.Get(ytEndpoint)
	if err != nil {
		return nil, err
	}

	result := &ytSearchResult{}

	defer resp.Body.Close()
	reader := bufio.NewReader(resp.Body)

	for {
		l, _, err := reader.ReadLine()
		// err does return io.EOF so in reality not all errors are equal in this case
		if err != nil {
			break
		}

		regexpRes := ytSearchPattern.FindSubmatch(l)
		if regexpRes != nil {
			id := string(regexpRes[1])
			title := string(regexpRes[3])
			duration := parseYtSearchDuration(string(regexpRes[5]))
			log.Println("SUKCES:", id, title, duration)
			entry := ytSearchResultEntry{Title: title, ID: id, Duration: duration}
			result.Entries = append(result.Entries, entry)

			if len(result.Entries) >= numberOfEntries {
				break
			}
		}
	}

	return result, nil
}

var urlPattern = regexp.MustCompile("^(http(s)?:\\/\\/)?(www\\.)?([0-9A-Za-z]+)\\.([a-z]{2,3})\\/(.+)")

func isURL(str string) bool {
	return urlPattern.MatchString(str)
}

var ytTitlePattern = regexp.MustCompile("<meta name=\"title\" content=\"(.*?)\">")
var ytDurationPattern = regexp.MustCompile("<meta itemprop=\"duration\" content=\"(.*?)\">")
var iso8601DurationPattern = regexp.MustCompile("^P(\\d+Y)?(\\d+M)?(\\d+W)?(\\d+D)?T?(\\d+H)?(\\d+M)?(\\d+S)?")

func parseDuration(value string) time.Duration {
	if len(value) == 0 {
		return 0
	}
	parsed, err := strconv.Atoi(value)
	if err != nil {
		return 0
	}
	return time.Duration(parsed)
}

var durations = [7]time.Duration{365 * 24 * time.Hour, 30 * 24 * time.Hour, 7 * 24 * time.Hour, 24 * time.Hour, time.Hour, time.Minute, time.Second}

func parseISO8601Duration(data []byte) time.Duration {
	regexpRes := iso8601DurationPattern.FindSubmatch(data)
	totalDuration := time.Duration(0)
	if regexpRes != nil {
		for i := 1; i < len(regexpRes); i++ {
			if len(regexpRes[i]) != 0 {
				toParse := string(regexpRes[i][:len(regexpRes[i])-1])
				totalDuration += durations[i-1] * parseDuration(toParse)
			}
		}
	}

	return totalDuration
}

func parseYtSearchDuration(data string) time.Duration {
	split := strings.Split(data, ":")
	n := len(split)
	totalDuration := time.Duration(0)
	totalDuration += time.Second * parseDuration(split[n-1][:len(split[n-1])-1])
	totalDuration += time.Minute * parseDuration(split[n-2])
	if n > 2 {
		totalDuration += time.Hour * parseDuration(split[n-3])
	}

	return totalDuration
}

func getYoutubeVideoInfo(url string) (*ytSearchResult, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	result := &ytSearchResult{}

	defer resp.Body.Close()
	reader := bufio.NewReader(resp.Body)

	title := ""
	duration := time.Duration(0)

	for {
		l, _, err := reader.ReadLine()
		// err does return io.EOF so in reality not all errors are equal in this case
		if err != nil {
			break
		}

		if title == "" {
			regexpRes := ytTitlePattern.FindSubmatch(l)
			if regexpRes != nil {
				title = string(regexpRes[1])
			}
		}

		if title != "" && duration == 0 {
			regexpRes := ytDurationPattern.FindSubmatch(l)
			if regexpRes != nil {
				duration = parseISO8601Duration(regexpRes[1])
			}
			if duration != 0 {
				entry := ytSearchResultEntry{Title: title, ID: url, Duration: duration}
				result.Entries = append(result.Entries, entry)
				break
			}
		}
	}

	return result, nil
}

func searchHandler(hd handlerData) (ctx *context) {
	var (
		arg = hd.arg
		u   = hd.user
		c   = hd.channel
		g   = hd.guild
		s   = hd.session
		b   strings.Builder
	)

	vsc := getUserVoiceChannel(u, g, s)
	if vsc == nil {
		fmt.Fprint(&b, "You have to be in a voice channel!\n")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	fmt.Fprint(&b, "MAM TO! Czekaj chwilę...\n")
	s.ChannelMessageSend(c.ID, b.String())

	searchTerm := arg
	if isURL(searchTerm) {
		log.Println("Getting video information from youtube", searchTerm)
		result, err := getYoutubeVideoInfo(searchTerm)
		if err != nil {
			log.Println("Error:", err)
			b.Reset()
			fmt.Fprintf(&b, "Failed to get results for \"%s\"\n", searchTerm)
			s.ChannelMessageSend(c.ID, b.String())
			return
		}

		hd.arg = "1"
		hd.data = result
		return playHandler(hd)
	}

	// TODO: get rid of code repetition
	log.Println("Searching youtube for", searchTerm)
	result, err := searchYoutubeFast(searchTerm)
	if err != nil {
		log.Println("Error:", err)
		b.Reset()
		fmt.Fprintf(&b, "Failed to get results for \"%s\"\n", searchTerm)
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	b.Reset()
	fmt.Fprintf(&b, "Results for \"%s\":\n", searchTerm)
	for i, e := range result.Entries {
		fmt.Fprintf(&b, "%d.\t%s\n", i+1, e.Title)
	}
	fmt.Fprintf(&b, "0.\tCancel\n")
	s.ChannelMessageSend(c.ID, b.String())

	ctx = &context{Timestamp: time.Now(), Data: result, HandleFunc: playHandler}
	return ctx
}

func init() {
	handlers["searchHandler"] = searchHandler
}
