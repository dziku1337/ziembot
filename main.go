package main

import (
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

var handlers = map[string]handler{}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + config.discordToken)
	if err != nil {
		log.Fatalln("Error creating Discord session:", err)
	}

	router, err := NewRouter(config)
	if err != nil {
		log.Fatalln("Error creating command router:", err)
	}

	for hKey, hFunc := range handlers {
		router.RegisterHandler(hKey, hFunc)
	}
	router.RegisterDiscordCallbacks(dg)

	// Open the websocket and begin listening.
	err = dg.Open()
	if err != nil {
		log.Fatalln("Error opening Discord session:", err)
	}

	// Wait here until CTRL-C or other term signal is received.
	log.Println("Ziembot is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}
