package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
)

type playerKey struct {
	ChannelID string
	GuildID   string
}

var players = make(map[playerKey]*Player)
var playersRWMutex sync.RWMutex

func playHandler(hd handlerData) (ctx *context) {
	var (
		arg  = hd.arg
		u    = hd.user
		c    = hd.channel
		g    = hd.guild
		s    = hd.session
		data = hd.data.(*ytSearchResult)
		b    strings.Builder
	)

	vsc := getUserVoiceChannel(u, g, s)
	if vsc == nil {
		fmt.Fprintln(&b, "You have to be in a voice channel!")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	selected, err := strconv.Atoi(arg)
	if err != nil {
		fmt.Fprintln(&b, "Couldn't parse selected index!")
		s.ChannelMessageSend(c.ID, b.String())
		return
	} else if selected == 0 {
		return
	} else if selected > len(data.Entries) {
		fmt.Fprintln(&b, "Index out of bounds!")
		s.ChannelMessageSend(c.ID, b.String())
		return
	}

	selected-- // 0 is "Cancel"
	e := data.Entries[selected]

	key := playerKey{ChannelID: vsc.ID, GuildID: g.ID}
	playersRWMutex.RLock()
	player, ok := players[key]
	playersRWMutex.RUnlock()
	if !ok {
		playersRWMutex.Lock()
		if player, ok = players[key]; !ok {
			player = NewPlayer(vsc, c, g)
			players[key] = player
		}
		playersRWMutex.Unlock()
	}

	player.Add(&e)
	log.Printf("Added \"%s-%s\" to the queue\n", e.Title, e.ID)
	fmt.Fprintf(&b, "Added \"%s\" to the queue\n", e.Title)
	s.ChannelMessageSend(c.ID, b.String())

	player.Play(s)
	return
}

func init() {
	handlers["playHandler"] = playHandler
}
