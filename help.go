package main

import (
	"fmt"
	"strings"
)

func helpHandler(hd handlerData) (ctx *context) {
	var (
		c = hd.channel
		s = hd.session
		b strings.Builder
	)

	fmt.Fprint(&b, "Prefixes:")
	for _, prefix := range config.Prefixes {
		fmt.Fprintf(&b, " %s", prefix)
	}
	fmt.Fprint(&b, "\n")

	fmt.Fprint(&b, "Commands:\n")
	for _, command := range config.Commands {
		fmt.Fprint(&b, "PREF ")
		for i, route := range command.Routes {

			if i == 0 {
				fmt.Fprintf(&b, "%s", route)
				if len(command.Routes) > 1 {
					fmt.Fprint(&b, " [")
				}
			} else if i < len(command.Routes)-1 {
				fmt.Fprintf(&b, "%s, ", route)
			} else {
				fmt.Fprintf(&b, "%s]", route)
			}
		}
		for _, arg := range command.Args {
			fmt.Fprintf(&b, " %s", arg)
		}

		fmt.Fprintf(&b, " - %s\n", command.Description)
	}

	s.ChannelMessageSend(c.ID, b.String())
	return
}

func init() {
	handlers["helpHandler"] = helpHandler
}
