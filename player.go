package main

import (
	"bytes"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"layeh.com/gopus"
)

// Player containing all data for playback on a voice channel
type Player struct {
	playlist             []*ytSearchResultEntry
	playlistMutex        sync.Mutex
	playlistNonEmpty     chan bool
	channel              *discordgo.Channel
	textChannel          *discordgo.Channel
	guild                *discordgo.Guild
	skipChannel          chan bool
	inactivityTimeoutSec int
	isPlaying            bool
	isPlayingMutex       sync.Mutex
	opusEncoder          *gopus.Encoder
}

// Creates new Player object
func NewPlayer(c *discordgo.Channel, tc *discordgo.Channel, g *discordgo.Guild) *Player {
	opusEncoder, _ := gopus.NewEncoder(frameRate, channels, gopus.Audio)
	return &Player{
		playlist:             []*ytSearchResultEntry{}, // I know it's O(n^2), relax
		playlistNonEmpty:     make(chan bool, 1),
		channel:              c,
		textChannel:          tc,
		guild:                g,
		skipChannel:          make(chan bool, 1),
		inactivityTimeoutSec: 300,
		isPlaying:            false,
		opusEncoder:          opusEncoder,
	}
}

// Playback goroutine
func (p *Player) Playback(s *discordgo.Session) {
	var (
		b                 bytes.Buffer
		inactivityTimeout chan bool
		toPlay            *ytSearchResultEntry
		voiceConnection   *discordgo.VoiceConnection
		err               error
	)

	log.Println("Entered playback func for channel", p.channel.Name)

	// Set the state to idle when leaving the function
	defer func() {
		p.isPlayingMutex.Lock()
		p.isPlaying = false
		p.isPlayingMutex.Unlock()
	}()

	for {
		log.Printf("Trying to pop from playlist for channel %s\n", p.channel.ID)
		toPlay, err = p.Pop()
		if err != nil {
			log.Printf("Playlist for guild %s channel %s turned out to be empty...\n", p.guild.ID, p.channel.ID)

			inactivityTimeout = make(chan bool, 1)
			go func() {
				time.Sleep(time.Duration(p.inactivityTimeoutSec) * time.Second)
				inactivityTimeout <- true
			}()

		inactivityLoop:
			for {
				log.Printf("Channel %s, inactivity loop\n", p.channel.ID)
				select {
				case <-p.playlistNonEmpty:
					// Check if there really is something to play
					log.Printf("Channel %s: Read from chan playlistNonEmpty\n", p.channel.ID)
					toPlay, err = p.Pop()
					if err == nil {
						break inactivityLoop
					}
				case <-inactivityTimeout:
					// Leave the voice channel if it was already there
					if voiceConnection != nil {
						voiceConnection.Disconnect()
						log.Println("Disconnected from the voice channel due to inactivity")
					}
					return
				}
			}
		}

		// Here we are sure we have something to play
		log.Printf("To play in channel %s: %s\n", p.channel.ID, toPlay.Title)
		inactivityTimeout = nil
		if voiceConnection == nil {
			log.Printf("Joining voice channel %s\n", p.channel.ID)
			voiceConnection, err = s.ChannelVoiceJoin(p.guild.ID, p.channel.ID, false, true)
			if err != nil {
				b.Reset()
				fmt.Fprintf(&b, "Failed to join voice channel %s\n", p.channel.Name)
				log.Printf("Failed to join voice channel %s\n", p.channel.Name)
				s.ChannelMessageSend(p.textChannel.ID, b.String())
				return
			}
		}

		s.UpdateStatus(0, toPlay.Title)
		b.Reset()
		fmt.Fprintf(&b, "Playing %s\n", toPlay.Title)
		log.Printf("Playing %s\n", toPlay.Title)
		s.ChannelMessageSend(p.textChannel.ID, b.String())

		p.playlistMutex.Lock()
		p.skipChannel = make(chan bool, 1)
		p.playlistMutex.Unlock()

		//err = streamYoutubeAudio(toPlay.ID, voiceConnection, p.skipChannel, p.opusEncoder)
		err = streamOpusWebm(toPlay.ID, voiceConnection, p.skipChannel)
		if err != nil {
			log.Println("Failed to stream audio:", err)
		} else {
			log.Printf("Finished playing %s\n", toPlay.Title)
		}
	}
}

// Starts playback if the player is not playing, otherwise it does nothing
func (p *Player) Play(s *discordgo.Session) {
	p.isPlayingMutex.Lock()
	defer p.isPlayingMutex.Unlock()
	if !p.isPlaying {
		p.isPlaying = true
		go p.Playback(s)
	}
}

// Skips currently played song. Does nothing if the player is not playing.
func (p *Player) Skip() {
	select {
	case p.skipChannel <- true:
	default:
	}
}

// Removes the playlist entry at given position
func (p *Player) Remove(position int) error {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	if position < 0 || position >= len(p.playlist) {
		return fmt.Errorf("Index %d is out of bounds for playlist of length %d", position, len(p.playlist))
	}
	p.playlist = append(p.playlist[:position], p.playlist[position+1:]...)
	return nil
}

// Removes all entries from the playlist
func (p *Player) Purge() {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	p.playlist = p.playlist[:0]
}

// Lists all entries in the playlist
func (p *Player) List() []*ytSearchResultEntry {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	return append([]*ytSearchResultEntry(nil), p.playlist...) // Copy - is it required?
}

// Returns number of entries in the playlist
func (p *Player) Size() int {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	return len(p.playlist)
}

// Adds an entry to the playlist
func (p *Player) Add(e *ytSearchResultEntry) {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	p.playlist = append(p.playlist, e)
	// TODO: Provide information that there is something in the playlist
	// so that the playback goroutine can know there's something to pop

	select {
	case p.playlistNonEmpty <- true:
		log.Println("Pushed to playlistNonEmpty for channel", p.channel.ID)
	default:
	}
}

// Returns and removes the first entry from the playlist
func (p *Player) Pop() (*ytSearchResultEntry, error) {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	log.Println("Length of playlist for channel", p.channel.ID, len(p.playlist))
	if len(p.playlist) == 0 {
		return nil, fmt.Errorf("Cannot pop from empty playlist")
	}
	front := p.playlist[0]
	p.playlist = p.playlist[1:]
	log.Printf("Front: %s, len rest %d\n", front.Title, len(p.playlist))
	return front, nil
}

// Shuffles the playlist
func (p *Player) Shuffle() {
	p.playlistMutex.Lock()
	defer p.playlistMutex.Unlock()
	rand.Shuffle(len(p.playlist), func(i, j int) {
		p.playlist[i], p.playlist[j] = p.playlist[j], p.playlist[i]
	})
}
